/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.*;

/**
 *
 * @author madarme
 */
public class Matriz_Excel_Ejemplo {

    private String  nombreArchivo;

    public Matriz_Excel_Ejemplo() {
    }

    public Matriz_Excel_Ejemplo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public char[][] leerExcel() throws Exception {
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(this.nombreArchivo));
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum() + 1;
        //Se crea la matriz con la cantidad de filas de la matriz en excel
        char[][] m = new char[canFilas][];
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            int cantCol = filas.getLastCellNum();
            //Por cada fila se crea la cantidad de columnas
            m[i] = new char[cantCol];
            for (int j = 0; j < cantCol; j++) {
                
                m[i][j] = filas.getCell(j).getStringCellValue().charAt(0);
            
            }

        }
        return m;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }
}
