package Negocio;

/**
 * Write a description of class SopaDeLetras here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetras {

    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras() {

    }

    public SopaDeLetras(String palabras) throws Exception {
        if (palabras == null || palabras.isEmpty()) {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:
        String palabras2[] = palabras.split(",");
        this.sopas = new char[palabras2.length][];
        int i = 0;
        for (String palabraX : palabras2) {
            //Creando las columnas de la fila i
            this.sopas[i] = new char[palabraX.length()];
            pasar(palabraX, this.sopas[i]);
            i++;

        }
    }
    public SopaDeLetras(char excel[][]){
        this.sopas = excel;
    }
    
    private void pasar(String palabra, char fila[]) {

        for (int j = 0; j < palabra.length(); j++) {
            fila[j] = palabra.charAt(j);
        }
    }

    public String toString() {
        String msg = "";
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                msg += this.sopas[i][j] + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public String toString2() {
        String msg = "";
        for (char filas[] : this.sopas) {
            for (char dato : filas) {
                msg += dato + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public boolean esCuadrada() {
        boolean esCuadrada = true;
        int c = this.sopas.length;
        for (int i = 0; i < this.sopas.length; i++) {
            if (c != this.sopas[i].length) {
                esCuadrada = false;
            }
            //if(this.sopa.length==sopa[0].length){ 
            // esCuadrada=true;
            //}
        }
        return esCuadrada;
    }

    public boolean esDispersa() {
        return (!this.esRectangular() && !this.esCuadrada());
    }

    public boolean esRectangular() {
        boolean esRectangular = true;
        for (int i = 0; i < sopas.length && esRectangular; i++) {
            if (sopas.length == sopas[i].length) {
                esRectangular = false;
            }
        }
        return esRectangular;
    }

    /*
        retorna cuantas veces esta la palabra en la matriz
     */
    public int getContar(String palabra) {
        return 0;
    }

    /*
        debe ser cuadrada sopas
     */
    public char[] getDiagonalPrincipal() throws Exception {//si y solo si es cuadrada , si no, lanza excepcion

        if (!esCuadrada()) {
            throw new Exception("Error, no es cuadrada.");
        }
        char diagoPrincipal[] = new char[sopas.length];
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                if (i == j) {
                    diagoPrincipal[i] = sopas[i][j];
                }
            }
        }
        return diagoPrincipal;
    }

//Start GetterSetterExtension Source Code
    /**
     * GET Method Propertie sopas
     */
    public char[][] getSopas() {
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
}
